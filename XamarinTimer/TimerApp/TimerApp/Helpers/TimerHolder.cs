﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimerApp.Helpers
{
    internal class TimerHolder
    {
        public bool Enabled { get; set; }
        public TimerHolder()
        {
        }

        public void Start(double interval, Action callBack)
        {
            Enabled = true;
            Xamarin.Forms.Device.StartTimer(TimeSpan.FromMilliseconds(interval), delegate
            {
                if (Enabled)
                {
                    callBack();
                }
                return Enabled;
            });
        }
        public void Stop()
        {
            Enabled = false;
        }
    }
}
