﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Input;
using TimerApp.Helpers;
using TimerApp.Model;
using Xamarin.Forms;

namespace TimerApp.ViewModel
{
    public class MainViewModel: ObservableObject
    {

        public ICommand StartTimerCommand { get; private set; }
        public ICommand StopTimerCommand { get; private set; }

        #region  private field
        private bool isBusy;

        private StopwatchModel stopWatch;
        private TimerHolder aTimer;
        private int countdown;
        #endregion

        public MainViewModel()
        {
            aTimer = new TimerHolder();
            StartTimerCommand = new Command(StartTimer);
            StopTimerCommand = new Command(StopTimer);
            var setuop = new StopwatchModel
            {
                GeneralTime = new TimeSpan(0, 0, 0),
                EvenTimer = new TimeSpan(0, 0, 0),
                UnevenTimer = new TimeSpan(0, 0, 0)
            };
            stopWatch = setuop;
        }


        #region public
        public StopwatchModel StopWatch
        {
            get { return stopWatch; }
            set
            {
                stopWatch = value;
                OnPropertyChanged("StopWatch");
            }
        }

        public bool IsBusy
        {
            get { return !isBusy; }
            set
            {
                isBusy = value;
                OnPropertyChanged();
            }
        }

        public int Countdown
        {
            get
            {
                return countdown;
            }
            set
            {
                countdown = value;

                StopWatch.GeneralTime = TimeSpan.FromSeconds(value);
            }
        }

        #endregion

        #region private
        private void StartTimer()
        {
            IsBusy = true;
            aTimer.Start(1000, OnTimedEvent);
        }

        private void StopTimer()
        {
            aTimer.Stop();
            ResetTimer();
            IsBusy = false;
        }

        private void ResetTimer()
        {
            StopWatch = new StopwatchModel
            {
                GeneralTime = new TimeSpan(0, 0, 0),
                EvenTimer = new TimeSpan(0, 0, 0),
                UnevenTimer = new TimeSpan(0, 0, 0)
            };
            countdown = 0;
            OnPropertyChanged();
        }

        private void OnTimedEvent()
        {
            if (Countdown != 0 && stopWatch.GeneralTime == new TimeSpan(0, 0, 0))
            {
                StopTimer();
                return;
            }

            long ticks;

            if (Countdown == 0)
            {
                ticks = stopWatch.GeneralTime.Ticks + TimeSpan.TicksPerSecond;
            }
            else
            {
                ticks = stopWatch.GeneralTime.Ticks - TimeSpan.TicksPerSecond;
            }

            UpdateDisplayTimers(ticks);
        }

        private void UpdateDisplayTimers(long ticks)
        {
            TimeSpan currentTime = new TimeSpan(ticks);
            var displayTim = new StopwatchModel()
            {
                GeneralTime = currentTime
            };

            if (currentTime.Seconds % 2 == 0)
            {
                displayTim.EvenTimer = currentTime;
                displayTim.UnevenTimer = stopWatch.UnevenTimer;
            }
            else
            {
                displayTim.UnevenTimer = currentTime;
                displayTim.EvenTimer = stopWatch.EvenTimer;
            }

            StopWatch = displayTim;
        }
        #endregion

    }
}
