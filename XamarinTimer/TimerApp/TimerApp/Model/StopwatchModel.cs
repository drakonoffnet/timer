﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimerApp.Helpers;

namespace TimerApp.Model
{
    public class StopwatchModel : ObservableObject
    {
        private TimeSpan generalTime;
        private TimeSpan evenTimer;
        private TimeSpan unevenTimer;


        public TimeSpan GeneralTime
        {
            get
            {
                return generalTime;
            }
            set { SetProperty(ref generalTime, value); }
            
        }

        public TimeSpan EvenTimer
        {
            get
            {
                return evenTimer;
            }
            set
            {
                SetProperty(ref evenTimer, value);
            }
        }

        public TimeSpan UnevenTimer
        {
            get
            {
                return unevenTimer;
            }
            set
            {
                SetProperty(ref unevenTimer, value);
            }
        }

    }
}
