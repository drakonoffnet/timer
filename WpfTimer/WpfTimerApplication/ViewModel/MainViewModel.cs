using System;
using System.Text.RegularExpressions;
using System.Timers;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using WpfTimerApplication.Model;

namespace WpfTimerApplication.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        #region public field
        public ICommand StartTimerCommand { get; private set; }
        public ICommand StopTimerCommand { get; private set; }
        #endregion

        #region  private field
        private bool isBusy;

        private StopwatchModel stopWatch;
        private Timer aTimer;
        private int countdown;
        #endregion

        public MainViewModel()
        {
            StartTimerCommand = new RelayCommand(StartTimer);
            StopTimerCommand = new RelayCommand(StopTimer);
            var setuop = new StopwatchModel
            {
                GeneralTime = new TimeSpan(0, 0, 0),
                EvenTimer = new TimeSpan(0, 0, 0),
                UnevenTimer = new TimeSpan(0, 0, 0)
            };
            stopWatch = setuop;
            aTimer = new Timer(1000);
            isBusy = false;
        }
        #region public
        public StopwatchModel StopWatch
        {
            get { return stopWatch; }
            set
            {
                stopWatch = value;
                RaisePropertyChanged("StopWatch");
            }
        }

        public bool IsBusy
        {
            get { return !isBusy; }
            set
            {
                isBusy = value;
                RaisePropertyChanged("IsBusy");
            }
        }

        public int Countdown
        {
            get
            {
                return countdown;
            }
            set
            {
                countdown = value;
                
                StopWatch.GeneralTime = TimeSpan.FromSeconds(value);
            }
        }

        #endregion

        #region private
        private void StartTimer()
        {
            IsBusy = true;
            aTimer.Elapsed += OnTimedEvent;
            aTimer.Start();
        }

        private void StopTimer()
        {
            aTimer.Elapsed -= OnTimedEvent;
            aTimer.Stop();
            ResetTimer();
            IsBusy = false;
        }

        private void ResetTimer()
        {
            StopWatch = new StopwatchModel
            {
                GeneralTime = new TimeSpan(0, 0, 0),
                EvenTimer = new TimeSpan(0, 0, 0),
                UnevenTimer = new TimeSpan(0, 0, 0)
            };
            countdown = 0;
            RaisePropertyChanged("Countdown");
        }

        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            if (Countdown != 0 && stopWatch.GeneralTime == new TimeSpan(0, 0, 0))
            {
                StopTimer();
                return;
            }

            long ticks;

            if (Countdown == 0)
            {
                ticks = stopWatch.GeneralTime.Ticks + TimeSpan.TicksPerSecond;
            }
            else
            {
                ticks = stopWatch.GeneralTime.Ticks - TimeSpan.TicksPerSecond;
            }

            UpdateDisplayTimers(ticks);
        }

        private void UpdateDisplayTimers(long ticks)
        {
            TimeSpan currentTime = new TimeSpan(ticks);
            var displayTim = new StopwatchModel()
            {
                GeneralTime = currentTime
            };

            if (currentTime.Seconds % 2 == 0)
            {
                displayTim.EvenTimer = currentTime;
                displayTim.UnevenTimer = stopWatch.UnevenTimer;
            }
            else
            {
                displayTim.UnevenTimer = currentTime;
                displayTim.EvenTimer = stopWatch.EvenTimer;
            }

            StopWatch = displayTim;
        }
        #endregion
    }
}