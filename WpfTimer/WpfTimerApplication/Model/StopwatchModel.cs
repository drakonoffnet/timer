﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace WpfTimerApplication.Model
{
    public class StopwatchModel: ObservableObject
    {
        private TimeSpan generalTime;
        private TimeSpan evenTimer;
        private TimeSpan unevenTimer;


        public TimeSpan GeneralTime
        {
            get
            {
                return generalTime;
            }
            set
            {
                Set<TimeSpan>(() => this.GeneralTime, ref generalTime, value);
            }
        }

        public TimeSpan EvenTimer
        {
            get
            {
                return evenTimer;
            }
            set
            {
                Set<TimeSpan>(() => this.EvenTimer, ref evenTimer, value);
            }
        }

        public TimeSpan UnevenTimer
        {
            get
            {
                return unevenTimer;
            }
            set
            {
                Set<TimeSpan>(() => this.UnevenTimer, ref unevenTimer, value);
            }
        }

        
    }
}
